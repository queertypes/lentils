<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Lentils: The Smol Issue Tracker](#lentils-the-smol-issue-tracker)
- [Features](#features)
- [Non-features](#non-features)
- [Storage](#storage)
  - [Text](#text)
  - [SQLite](#sqlite)
  - [Caching](#caching)
- [Examples](#examples)
  - [Creating](#creating)
  - [Viewing](#viewing)
  - [Queries](#queries)
  - [Import/Export](#importexport)
  - [Listing Things](#listing-things)
- [Contributing](#contributing)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Lentils: The Smol Issue Tracker

* **status**: not yet started!
* **license**: GPLv3

`lentils` is a simple, CLI-based issue tracker. It makes it easy to
get started, can grow with your project, and works with your version
control system.

# Features

* Comprehensive issue data model
* Customizable storage: plain/text, sqlite
* Caching for faster start up, updated when issues are updated
* Guided creation of:
  * Lentils Tracker setup for a new project
  * Issues
  * Milestones
* Flexible issue search:
  * key:value specification
  * key1:value1 key2:value2
  * key:\^not-value
  * key:(value1 | value2)
  * key:(%regex-value)
* Issue import/export:
  * `lentils` bundle
  * CSV
  * JSON
  * YAML
  * XML
  * sqlite
  * More?
* Version control system integration (branches/tags/users):
  * [git](https://git-scm.com/)
  * [mercurial](https://www.mercurial-scm.org/)
  * [pijul](http://pijul.org/)
  * More?
* Editor integration (add/edit/view issues):
  * [emacs](https://www.gnu.org/software/emacs/)
  * [vim](http://www.vim.org/)
  * More?

# Non-features

These are specific things that `lentils` will never do, but may be
added by a separate project.

* External storage:
  * `lentils` is about leveraging existing DCVS tooling
* Analytics
  * A separate project may build on lentils for metrics generation
* GUI Interface
  * `lentils` is scoped to terminal interaction
  * Additional projects may add curses/GUI/web/etc. views

# Storage

## Text

Simple. Can be edited with a text editor. Depending on local storage
medium, may be fast enough for even large projects.

## SQLite

Complex. Requires using `lentils` CLI for editing issues. Aims to be
faster than plain text.

## Caching

To speed up command invocations, `lentils` maintains a local cache of
all issue metadata in a format that avoids the need for text
parsing. This cache is validated on start up, compared to the issue
store hash, and proceeds from there.

# Examples

## Creating

* Creating a new issue using a guide:

```
$ lentils add --guided
TODO
```

* Creating a new issue using parameters:

```
$ lentils add\
  --title 'So good'
  ...

Issue noted!
  title: 'So good'
  branch: so-good-1
  type: docs
  priority: low
```

## Viewing

* Viewing an issue:

```
$ lentils view title:'So'
  title: 'So good'
  branch: 'so-good-1'
  type: docs
  created: 2016-04-26 00:32:32+00:00
  created-by: Allele Dev <allele.dev@gmail.com>
  updated: 2016-04-26 00:32:32+00:00
  updated-by: Allele Dev <allele.dev@gmail.com>
  priority: low
  state: unassigned
  release-status: unreleased
  milestones:
  tags:
  ...
```

* Viewing an issue using a predefined formatter:

```
$ tissue view --format=oneline title:'So'
So Good|so-good-1|docs|2016-04-26 00:32:32+00:00|Allele Dev <allele.dev@gmail.com>|2016-04-26 00:32:32+00:00|Allele Dev <allele.dev@gmail.com>|low|unassigned|unreleased||...|
```

* Viewing documentation on how to format issues for viewing:

```
$ tissue view --help=format

Formating issues:
  %%  - a literal % sign
  %T - issue title
  %B - issue branch
  %t - issue type
  %C - issue creation time
  %c - issue created by
  %U - issue update time
  %u - issue updated by
  %P - issue priority
  %A - issue assigned to
  %S - issue state
  %R - issue release status
  %g - issue tags
  %M - issue milestones

Formatting timestamps:
  Timestamps default to ISO8601 combined date-time in UTC.

  Read more here for custom formats:
    https://hackage.haskell.org/package/time/docs/Data-Time-Format.html#v:formatTime

  Or, on a system with manpages installed:
    $ man date

  You can specify the timestamp format for created and updated times
  together or separately:

    --time-created-format=...
    --time-updated-format=...
    --time-format=...

  Priority is given to the inidividual format specifier.

Predefined formats:
  detailed: (default) one field per line, k:v format
  oneline: all issue fields in one line, separated by |
  summary: title, date updated, type, priority, assigned-to, state

Examples:
  # Title, Branch, Type, Date Created (Month dd, yyyy)
  $ tissue view --format='%T, %B, %t, %C'\
      --time-created-format='%B %d, %Y'
  So Good, so-good-1, docs, April 26, 2016
```

* Viewing documentation on querying the issue store:

```
$ lentils view --help=query
TODO
```

## Queries

* Simple queries for finding issues

```
$ lentils state:unassigned
TODO
```

## Import/Export

* Exporting issues:

```
$ lentils export --format=csv
TODO
```

* Exporting a subset of all issues:

```
$ lentils export --format=csv type:docs
TODO
```

* Importing issues:

```
$ lentils import external-issues.csv
TODO
$ lentils import http://git.my-other-site.com/a-project/external-issues.csv
TODO
$ lentils import https://git.my-other-site.com/a-project/external-issues.csv
TODO
$ lentils import git:user@host:a-project/external-issues.csv
TODO
$ lentils import pijul:user@host:a-project/external-issues.csv
TODO
```

## Listing Things

* Listing known milestone tags:

```
$ lentils list milestones
TODO
```

* Listing known tags:

```
$ lentils list tags
TODO
```

* Listing known users, contributors:

```
$ lentils list users
TODO
```

* Listing branches `lentils` created and is managing:

```
$ lentils list branches
TODO
```

# Contributing

Be sure to review the included
[CODE_OF_CONDUCT.md](./CODE_OF_CONDUCT.md), based on the
[Contributor's Covenant](http://contributor-covenant.org/).

More details later!
